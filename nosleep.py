'''
This script will use the autopy module to move the mouse effectively
keeping windows from sleeping or locking.

Moves the mouse every 60 seconds using the task and reactor from the
twisted framework.
'''

import autopy
from twisted.internet import task
from twisted.internet import reactor

timeout = 5.0 # Sixty seconds

def move_mouse():
    mouse_location = autopy.mouse.get_pos()
    x = mouse_location[0]
    y = mouse_location[1]
    x = x + 5
    y = y + 5
    autopy.mouse.move(x, y)
    pass

l = task.LoopingCall(move_mouse)
l.start(timeout) # call every sixty seconds

reactor.run()
